'use strict';
/**
 * RS Exercise - URL Parser
 * @param {string} format - URL Format. For example: '/:version/api/:collection/:id'
 * @param {string} instance - URL Instance. For example: '/6/api/listings/3?sort=desc&limit=10'
 * @param {Boolean} [protocol] - Defines if URL Format and Instance have protocol section. False as default.
 * @param {Boolean} [domain] - Defines if URL Format and Instance have domain section. False as default.
 */

 export function urlParser(format, instance, protocol = false, domain = false) {

    // TO-DO: Validations
    // TO-DO: Implement clearProtocol()
    // TO-DO: Implement clearDomain()
    
    const formatArr = format.split('/').filter(i => i);
    const instancePathArr = instance.split("?")[0].split('/').filter(i => i);
    const instanceSearch = instance.split("?")[1] ? instance.split("?")[1].replace('?',"") : undefined;
    let result = {};

    formatArr.forEach((e, i) => {
        if (e.slice(0,1) == ":") {
            result[e.slice(1,e.length)] = instancePathArr[i];
        }
    })

    if (instanceSearch) {
        Object.assign(result, Object.fromEntries(new Map(instanceSearch.split("&").map(p => p.split("=")))));
    }

    return result;
}
