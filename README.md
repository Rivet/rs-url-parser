# rs-url-parser

## Index

* [Requirements](#requirements)
* [Download & Install](#download-and-install)
* [Usage](#usage)

## Requirements
 - Node version 14.17.4
 - Npm version 6.14.14

## Download and Install

Use the following command to install RS Url Parser

### NPM
```bash
npm install rs-url-parser
```

### Source Code
```bash
$ git clone https://gitlab.com/Rivet/rs-url-parser.git
$ cd rs-url-parser
$ npm install
```

## Usage

### Require url-parser
```js
const parser = require('url-parser.js')
```

>Now you can call the `parser` method with two required parameters. The first one is for 
the URL format and the second one is for the URL instance.
- `format` (`String`): URL Format string. For example: '/:var1/static/:var2/:var3'
- `instance` (`String`): URL Instance. For example: '/3/products/categories/24?sort=asc&qty=50'

### Call parser method
```js
let url = parser('/:var1/static/:var2/:var3', '/3/products/categories/24?sort=asc&qty=50');
```

>In this beta version, is assumed that you are passing only the path section of the URL 
(without protocol and domain). In next update, protocol and or domain will be accepted.

### Results
>It will return an object with the variable parts (those starting with `:`) matching its 
instance values; and all keys and values in `queryString` if provided.

For the example above, the returning object will look like this:

```json
    {
       "var1": "3",
       "var2": "categories",
       "var3": "24",
       "sort": "asc",
       "qty" : "50"
    }
```